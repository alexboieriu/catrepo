package com.service;

import facade.ICatFacade;
import model.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 9/10/15.
 */
@Controller
@RequestMapping("/article")
public class ArticleService {

    @Autowired
    private ICatFacade catFacade;

    @RequestMapping(value="", method= RequestMethod.GET)
    @ResponseBody
    public List<Cat> getAllCats() {
        return this.catFacade.bringCats();
    }

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    @ResponseBody
    public Cat getCat(@PathVariable("id") Long catId) {
        return this.catFacade.getCat(catId);
    }

    @RequestMapping(value="/{id}", method= RequestMethod.PUT)
    @ResponseBody
    public void updateCat(@PathVariable("id")Long catId, @RequestBody Cat catUpdates) {
        catUpdates.setId(catId);
        this.catFacade.updateCat(catUpdates);
    }

    @RequestMapping(value="", method= RequestMethod.POST)
    @ResponseBody
    public void addCat(@RequestBody Cat newCat){
        this.catFacade.saveCat(newCat);
    }

    @RequestMapping(value="/{id}", method= RequestMethod.DELETE)
    @ResponseBody
    public void deleteCat(@PathVariable("id")Long catId){
        this.catFacade.deleteCat(catId);
    }

    public ICatFacade getCatFacade() {
        return catFacade;
    }

    public void setCatFacade(ICatFacade catFacade) {
        this.catFacade = catFacade;
    }
}
