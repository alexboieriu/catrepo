package com.service;

import facade.ICommentFacade;
import model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by root on 9/15/15.
 */
@Controller
public class CommentService {
    @Autowired
    private ICommentFacade commentFacade;

    @RequestMapping(value="/article/{articleId}/comment", method= RequestMethod.GET)
    @ResponseBody
    public List<Comment> getAllComments(@PathVariable("articleId") Long articleId) {
        return this.commentFacade.getAllComments(articleId);
    }


    @RequestMapping(value="/article/{articleId}/comment/{commentId}", method= RequestMethod.GET)
    @ResponseBody
    public Comment getComment(@PathVariable("articleId") Long articleId, @PathVariable("commentId") Long commentId) {
        return this.commentFacade.getComment(articleId, commentId);
    }

    @RequestMapping(value="/article/{articleId}/comment", method= RequestMethod.POST)
    @ResponseBody
    public void addComment(@PathVariable("articleId") Long articleId, @RequestBody Comment comment) {
        comment.setCatId(articleId);
        this.commentFacade.saveComment(comment);
    }


    @RequestMapping(value="/article/{articleId}/comment/{commentId}", method= RequestMethod.PUT)
    @ResponseBody
    public void updateComment(@PathVariable("articleId") Long articleId, @PathVariable("commentId") Long commentId, @RequestBody Comment comment) {
        comment.setId(commentId);
        comment.setCatId(articleId);
        this.commentFacade.updateComment(comment);
    }

    @RequestMapping(value="/article/{articleId}/comment/{commentId}", method= RequestMethod.DELETE)
    @ResponseBody
    public void updateComment(@PathVariable("articleId") Long articleId, @PathVariable("commentId") Long commentId) {
        this.commentFacade.deleteComment(articleId, commentId);
    }



    public ICommentFacade getCommentFacade() {
        return commentFacade;
    }

    public void setCommentFacade(ICommentFacade commentFacade) {
        this.commentFacade = commentFacade;
    }
}
