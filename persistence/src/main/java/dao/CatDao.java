package dao;

import model.Cat;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.*;
import java.util.List;

/**
 * Created by root on 9/11/15.
 */
public class CatDao implements ICatDao{

    private EntityManager entityManager;

    @PersistenceContext
    private void setEntityManager(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public List<Cat> getAll(){
        return this.entityManager.createQuery("from Cat").getResultList();
    }

    public Cat getCat(Long catId) {
        return this.entityManager.find(Cat.class, catId);
    }

    @Transactional
    public void updateCat(Cat cat) {
       Cat catFromDb = this.getCat(cat.getId());
        if (catFromDb != null) {
            catFromDb.setName(cat.getName());
            catFromDb.setAge(cat.getAge());
            catFromDb.setFur(cat.getFur());
            catFromDb.setWeight(cat.getWeight());
            this.entityManager.persist(catFromDb);
        }
    }
    @Transactional
    public void saveCat(Cat cat) {
        this.entityManager.persist(cat);
    }

    @Transactional
    public void deleteCat(Long catId) {
        Cat catFromDb = this.getCat(catId);
        if (catFromDb != null) {
            this.entityManager.remove(catFromDb);
        }
    }
}
