package dao;

import model.Comment;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by root on 9/15/15.
 */
public class CommentDao implements ICommentDao {

    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Comment> getAllComments(Long articleId) {
        Query query = this.entityManager.createQuery("from Comment where catId=:catId");
        query.setParameter("catId", articleId);
        return  query.getResultList();
    }

    public Comment getComment(Long articleId, Long commentId) {
        Query query = this.entityManager.createQuery("from Comment WHERE catId=:catId AND  id=:commentId");
        query.setParameter("catId", articleId);
        query.setParameter("commentId", commentId);
        List<Comment> result = query.getResultList();
        if (result != null && !result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }

    @Transactional
    public void saveComment(Comment comment) {
        this.entityManager.persist(comment);
    }

    @Transactional
    public void updateComment(Comment comment) {
        Comment commentFromDb = this.getComment(comment.getCatId(), comment.getId());
        if (commentFromDb != null) {
            commentFromDb.setContent(comment.getContent());
            this.entityManager.persist(commentFromDb);
        }
    }

    @Transactional
    public void deleteComment(Long articleId, Long commentId) {
        Comment commentFromDb = this.getComment(articleId, commentId);
        if (commentFromDb != null) {
            this.entityManager.remove(commentFromDb);
        }
    }
}
