package dao;

import model.Cat;

import java.util.List;

/**
 * Created by root on 9/11/15.
 */
public interface ICatDao {
    List<Cat> getAll();
    Cat getCat(Long catId);
    void updateCat(Cat cat);
    void saveCat(Cat cat);
    void deleteCat(Long catId);
}
