package facade;

import model.Comment;

import java.util.List;

/**
 * Created by root on 9/15/15.
 */
public interface ICommentFacade {
    public List<Comment> getAllComments(Long articleId);
    public Comment getComment(Long articleId, Long commentId);
    public void saveComment(Comment comment);
    public void updateComment(Comment comment);
    public void deleteComment(Long articleId, Long commentId);
}
