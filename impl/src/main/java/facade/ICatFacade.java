package facade;

import model.Cat;

import java.util.List;

/**
 * Created by root on 9/11/15.
 */
public interface ICatFacade {
    List<Cat> bringCats();
    Cat getCat(Long catId);
    void updateCat(Cat cat);
    void saveCat(Cat cat);
    void deleteCat(Long catId);
}
