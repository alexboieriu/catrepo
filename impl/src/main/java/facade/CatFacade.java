package facade;

import dao.ICatDao;
import model.Cat;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by root on 9/11/15.
 */
public class CatFacade implements ICatFacade{

    @Autowired
    private ICatDao catDao;

    public List<Cat> bringCats(){
        return this.catDao.getAll();
    }

    public Cat getCat(Long catId) {
        return this.catDao.getCat(catId);
    }

    public void updateCat(Cat cat) {
        this.catDao.updateCat(cat);
    }

    public void saveCat(Cat cat) {
        this.catDao.saveCat(cat);
    }

    public void deleteCat(Long catId) {
        this.catDao.deleteCat(catId);
    }

    public ICatDao getCatDao() {
        return catDao;
    }

    public void setCatDao(ICatDao catDao) {
        this.catDao = catDao;
    }
}
